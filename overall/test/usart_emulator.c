#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <signal.h>
/*
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
*/
#include <pthread.h>

#define USART1_PORT 5777
#define USART2_PORT 5778

uint8_t program_stop = 0;
uint8_t printBuf = 0;

pthread_t usart_thread[2];

uint32_t bauds[2] = {9600, 115200};

/* parity - none, odd */
uint8_t parity[2] = {0, 1};

/* stop duration - two bits, one bit */
uint8_t stopDur[2] = {2, 0};

uint8_t pin[2] = {0, 0};

void *usart1_thread(void *arg)
{
    uint32_t i, j;

    uint8_t data[12] = "Hello world\0";
    uint32_t size = 12;

    while (!program_stop)
    {
        uint8_t parity_sum = 0;
        uint32_t baudUs = 1000000 / bauds[0];

        uint8_t stopUs = baudUs;

        if (stopDur[0] == 1)
        {
            stopUs += (baudUs / 2);
        }
        else if (stopDur[0] == 2)
        {
            stopUs += baudUs;
        }

        /* bytes to bauds conversion */
        for (i = 0; i < size; i++)
        {
            /* send start bit (0) */
            pin[0] = 0;

            usleep(baudUs);

            for (j = 0; j < 7; j++)
            {
                //grab a bit and set USART pin
                pin[0] = (data[i] >> (7 - j)) & 0x01;

                //calc parity
                parity_sum ^= pin[0];

                usleep(baudUs);
            }

            if (parity[0] == 1)
            {
                pin[0] = parity_sum == 0;
                usleep(baudUs);
            }
            else if (parity[0] == 2)
            {
                pin[0] = parity_sum == 1;
                usleep(baudUs);
            }

            /* send stop bits (1) */
            pin[0] = 1;
            usleep(stopUs);
        }

        sleep(5);
    }

    return NULL;
}

void *usart2_thread(void *arg)
{
    uint8_t buffer[1024];
    uint32_t cur = 0;

    memset(buffer, 0, 1024);

    while (!program_stop)
    {
        uint8_t byte = 0;

        uint8_t st_found    = 0;
        uint8_t end_found   = 0;
        uint8_t paring      = 0;
        uint8_t success     = 0;
        uint8_t bit_pos     = 0;

        // in microseconds (and expect some accuracy)
        uint32_t baudUs = 1000000 / bauds[1] + 1;

        while (1)
        {
            /* byte was constructed */
            if (st_found && end_found)
            {
                st_found = end_found = bit_pos = 0;

                if (success)
                {
                    buffer[cur] = byte;
                    cur++;
                    break;
                }
            }

            if (pin[1] == 0 && !st_found)
            {
                st_found = 1;
                usleep(baudUs);
                continue;
            }

            if (bit_pos == 8)
            {
                if (parity[1] == 1 && paring % 2 == 1)
                {
                    success = (paring == pin[1]);
                }

                end_found = 1;
                usleep(baudUs);
                continue;
            }

            if (st_found && !end_found)
            {
                byte |= pin[1] << (7 - bit_pos);
                paring ^= pin[1];
                bit_pos++;
                usleep(baudUs);
            }

        }

        printf("%s\n", buffer);
        cur = 0;
    }

    return NULL;
}

int main(int argc, char *argv[])
{
    int sockfd[2];

    char chr = 0;

    while (1)
    {
        scanf("%c", &chr);

        if (chr == 'C')
        {
            break;
        }
    }

/*
    while (program_stop)
    {
        send(sockfd[0], pin[0], 1, 0);
        recv(sockfd[1], pin[1], 1, 0);
    }
*/
/*
    pthread_join(usart_thread[0]);
    pthread_join(usart_thread[1]);
*/
    return 0;
}
