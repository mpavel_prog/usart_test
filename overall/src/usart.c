#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <usart.h>

int32_t usart_init(usart_t *usart, uint8_t mode)
{
    /* Args check */
    if (!usart || mode > USART_MODE_TX)
    {
        return -1;
    }

    /* Re-init case */
    if (usart->mode != USART_MODE_NONE)
    {
        return -1;
    }

    usart->mode = mode;

    return 0;
}

int32_t usart_destroy(usart_t *usart)
{
    /* Args check */
    if (!usart)
    {
        return -1;
    }

    /* Re-destroy case */
    if (usart->mode == USART_MODE_NONE)
    {
        return -1;
    }
   
    usart->mode = USART_MODE_NONE;

    return 0;
}

int32_t usart_setCfg(usart_t *usart, usart_cfg_t cfg)
{
    /* Args check */
    if (!usart)
    {
        return -1;
    }

    int32_t res = 0;

    res |= usart_setBaudrate(usart, cfg.baudrate);
    res |= usart_setParity(usart, cfg.parity);
    res |= usart_setStopDur(usart, cfg.stopDur);

    return 0;
}

int32_t usart_setBaudrate(usart_t *usart, uint32_t baudrate)
{
    /* Args check */
    if (!usart || baudrate == 0)
    {
        return -1;
    }

    /* Check if USART was inited */
    if (usart->mode == USART_MODE_NONE)
    {
        return -1;
    }

    usart->cfg.baudrate = baudrate;

    return 0;
}

int32_t usart_setParity(usart_t *usart, UsartParityType parity)
{
    /* Args check */
    if (!usart || parity > PARITY_EVEN)
    {
        return -1;
    }

    /* Check if USART was inited */
    if (usart->mode == USART_MODE_NONE)
    {
        return -1;
    }

    usart->cfg.parity = parity;

    return 0;
}

int32_t usart_setStopDur(usart_t *usart, UsartStopDuration stopDur)
{
    /* Args check */
    if (!usart || stopDur > STOP_TWO)
    {
        return -1;
    }

    /* Check if USART was inited */
    if (usart->mode == USART_MODE_NONE)
    {
        return -1;
    }

    usart->cfg.stopDur = stopDur;

    return 0;
}

int32_t usart_setCb(usart_t *usart, UsartCbType type, uint32_t (*cb)(usart_cb_arg_t*))
{
    /* Args check */
    if (!usart || type > CB_TYPE_RX || !cb)
    {
        return -1;
    }

    /* Check if USART was inited */
    if (usart->mode == USART_MODE_NONE)
    {
        return -1;
    }

    if (type == CB_TYPE_TX)
    {
        usart->cbTx = cb;
    }
    else
    {
        usart->cbRx = cb;
    }

    return 0;
}

/* Blocking method & bit-banging */
int32_t usart_send(usart_t *usart, uint8_t *data, uint32_t size)
{
    /* Args check */
    if (!usart || !data || size == 0)
    {
        return -1;
    }

    /* Check if USART was inited */
    if (usart->mode == USART_MODE_NONE)
    {
        return -1;
    }

    uint32_t i, j;

    /* some external place like memory cell or anything to bang USART pin */
    uint8_t bit_val;

    uint8_t parity_sum = 0;

    uint32_t baudUs = 1000000 / usart->cfg.baudrate;

    /* Implying STOP_ONE is default value */
    uint8_t stopUs = baudUs;

    if (usart->cfg.stopDur == STOP_ONE_HALF)
    {
        stopUs += (baudUs / 2);
    }
    else if (usart->cfg.stopDur == STOP_TWO)
    {
        stopUs += baudUs;
    }

    /* bytes to bauds conversion */
    for (i = 0; i < size; i++)
    {
        /* send start bit (0) */
        bit_val = 0;
        usleep(baudUs);

        for (j = 0; j < 7; j++)
        {
            //grab a bit and set USART pin
            bit_val = (data[i] >> (7 - j)) & 0x01;

            //calc parity
            parity_sum ^= bit_val;

            usleep(baudUs);
        }

        if (usart->cfg.parity == PARITY_ODD)
        {
            bit_val = parity_sum == 0;
            usleep(baudUs);
        }
        else if (usart->cfg.parity == PARITY_EVEN)
        {
            bit_val = parity_sum == 1;
            usleep(baudUs);
        }

        /* send stop bits (1) */
        bit_val = 1;
        usleep(stopUs);
    }

    return 0;
}
