#ifndef _USART_H_
#define _USART_H_

#include <stdint.h>             /* c99 standard complied */

#define USART_MODE_NONE 0x00
#define USART_MODE_RX   0x01
#define USART_MODE_TX   0x02

typedef enum UsartCbType
{
    CB_TYPE_TX = 0,
    CB_TYPE_RX,
}UsartCbType;

typedef enum UsartParityType
{
    PARITY_NONE = 0,
    PARITY_ODD,
    PARITY_EVEN,
}UsartParityType;

typedef enum UsartStopDuration
{
    STOP_ONE = 0,
    STOP_ONE_HALF,
    STOP_TWO,
}UsartStopDuration;

typedef struct usart_cb_arg_t
{
    uint8_t     *data;
    uint32_t    *size;
}usart_cb_arg_t;

typedef struct usart_cfg_t
{
    uint32_t            baudrate;
    UsartParityType     parity;
    UsartStopDuration   stopDur;
}usart_cfg_t;

typedef struct usart_t
{
    uint8_t     fd;                            /* <- piece of memory/something to punch */
    uint8_t     mode;
    usart_cfg_t cfg;
    uint32_t    (*cbTx)(usart_cb_arg_t *arg);
    uint32_t    (*cbRx)(usart_cb_arg_t *arg);
}usart_t;

#define USART_EMPTY_INIT (usart_t){ }

/* (De/-)Init USART functions block */
extern int32_t usart_init(usart_t *usart, uint8_t mode);
extern int32_t usart_destroy(usart_t *usart);

/* Configuration functions block */
extern int32_t usart_setCfg(usart_t *usart, usart_cfg_t cfg);
extern int32_t usart_setBaudrate(usart_t *usart, uint32_t baudrate);
extern int32_t usart_setParity(usart_t *usart, UsartParityType parity);
extern int32_t usart_setStopDur(usart_t *usart, UsartStopDuration stopDur);

/* OPs functions block */
extern int32_t usart_setCb(usart_t *usart, UsartCbType type, uint32_t (*cb)(usart_cb_arg_t*));

extern int32_t usart_send(usart_t *usart, uint8_t *data, uint32_t size);
extern int32_t usart_recv(usart_t *usart, uint8_t *data, uint32_t *size);

#endif      /* _USART_H_ */
