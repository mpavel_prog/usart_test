#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <usart.h>

void init_block(usart_t *usart1, usart_t *usart2)
{
    usart_cfg_t cfg;

    /* Init block for USART1 */
    usart_init(usart1, USART_MODE_RX);
    cfg.baudrate = 9600;
    cfg.parity = PARITY_NONE;
    cfg.stopDur = STOP_TWO;
    usart_setCfg(usart1, cfg);

    /* Init block for USART2 */
    usart_init(usart2, USART_MODE_TX);
    cfg.baudrate = 115200;
    cfg.parity = PARITY_ODD;
    cfg.stopDur = STOP_ONE;
    usart_setCfg(usart2, cfg);
}

void destroy_block(usart_t *usart1, usart_t *usart2)
{
    usart_destroy(usart1);
    usart_destroy(usart2);
}

int main(int argc, char *argv[])
{
    uint8_t buffer[128];

    usart_t usart1 = USART_EMPTY_INIT;
    usart_t usart2 = USART_EMPTY_INIT;

    init_block(&usart1, &usart2);

    usart_send(&usart2, buffer, 128);

    destroy_block(&usart1, &usart2);

    return 0;
}
